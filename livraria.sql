/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.24-log : Database - livraria
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`livraria` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `livraria`;

/*Table structure for table `author` */

DROP TABLE IF EXISTS `author`;

CREATE TABLE `author` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `author` */

insert  into `author`(`id`,`name`,`created_at`,`updated_at`) values (1,'Felipe Pieretti Umpierre','2014-11-23 17:34:47','2014-11-23 17:34:52');

/*Table structure for table `book` */

DROP TABLE IF EXISTS `book`;

CREATE TABLE `book` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `book_language_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `file_upload` varchar(255) DEFAULT NULL,
  `number_page` int(4) DEFAULT NULL,
  `copy` int(3) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `book` */

insert  into `book`(`id`,`category_id`,`book_language_id`,`name`,`file_upload`,`number_page`,`copy`,`year`,`created_at`,`updated_at`) values (1,2,1,'Cinquenta Tons de Cinza','livro-1.jpg',100,3,2004,'2014-11-09 15:11:37','2014-12-04 14:22:18'),(2,1,1,'Felicidade Roubada - Um Romance Psicológico Sobre Os Fantasmas da Emoção','livro-2.png',435,6,2012,'2014-11-13 22:38:00','2014-11-24 16:15:26'),(3,1,1,'A Escolha - da Trilogia A Seleção - Vol. 3','livro-3.jpg',234,12,0,'2014-11-13 22:40:44','2014-12-03 16:11:32');

/*Table structure for table `book_author` */

DROP TABLE IF EXISTS `book_author`;

CREATE TABLE `book_author` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `book_id` int(11) DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `book_author` */

insert  into `book_author`(`id`,`book_id`,`author_id`) values (1,1,1),(2,2,1),(3,3,1);

/*Table structure for table `book_description` */

DROP TABLE IF EXISTS `book_description`;

CREATE TABLE `book_description` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` int(11) DEFAULT NULL,
  `mini_description` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `book_description` */

insert  into `book_description`(`id`,`book_id`,`mini_description`,`description`,`created_at`,`updated_at`) values (1,1,'Quando Anastasia Steele entrevista o jovem empresário Christian Grey, descobre nele um homem atraente, brilhante e profundamente dominador.','Quando Anastasia Steele entrevista o jovem empresário Christian Grey, descobre nele um homem atraente, brilhante e profundamente dominador. Ingênua e inocente, Ana se surpreende ao perceber que, a despeito da enigmática reserva de Grey, está desesperadamente atraída por ele. Incapaz de resistir à beleza discreta, à timidez e ao espírito independente de Ana, Grey admite que também a deseja - mas em seu próprios termos...','2014-11-24 12:06:20','2014-11-24 10:06:20'),(2,2,'','','2014-11-24 12:56:35','2014-11-24 10:56:35'),(3,3,'','','2014-11-24 12:57:02','2014-11-24 10:57:02');

/*Table structure for table `book_language` */

DROP TABLE IF EXISTS `book_language`;

CREATE TABLE `book_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `book_language` */

insert  into `book_language`(`id`,`name`,`created_at`,`updated_at`) values (1,'Português',NULL,'2014-11-24 09:59:46'),(2,'Inglês',NULL,'2014-11-24 09:59:49');

/*Table structure for table `category` */

DROP TABLE IF EXISTS `category`;

CREATE TABLE `category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL DEFAULT '',
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `category` */

insert  into `category`(`id`,`name`,`created_at`,`updated_at`) values (1,'Ação','2014-11-09 15:12:07','2014-12-03 18:19:58'),(2,'Ficção',NULL,NULL);

/*Table structure for table `reserved` */

DROP TABLE IF EXISTS `reserved`;

CREATE TABLE `reserved` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `book_id` int(11) DEFAULT NULL,
  `users_id` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `reserved` */

insert  into `reserved`(`id`,`book_id`,`users_id`,`status_id`,`created_at`,`updated_at`) values (1,1,1,1,'2014-12-03 14:04:50','2014-12-03 14:04:57'),(2,3,1,1,'2014-12-04 12:28:08',NULL),(3,1,1,1,'2014-12-04 12:28:08',NULL);

/*Table structure for table `status` */

DROP TABLE IF EXISTS `status`;

CREATE TABLE `status` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL DEFAULT '',
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `status` */

insert  into `status`(`id`,`name`,`created_at`,`updated_at`) values (1,'Pendente','2014-12-03 13:25:38','2014-12-03 13:25:38'),(2,'Esperando pagamento','2014-12-03 13:25:45','2014-12-03 13:26:02'),(3,'Pago','2014-12-03 13:25:46','2014-12-03 13:26:01'),(4,'Devolvido','2014-12-03 13:25:46','2014-12-03 13:25:57'),(5,'Atrasado','2014-12-03 13:25:47','2014-12-03 13:25:59');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT '',
  `cpf` int(15) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`cpf`,`phone`,`password`,`created_at`,`updated_at`) values (1,'Felipe Pieretti Umpierres','felipeumpierre@hotmail.com',2147483647,'+555197968141','202cb962ac59075b964b07152d234b70','2014-12-02 13:49:42','2014-12-04 10:01:45');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
