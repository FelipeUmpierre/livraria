<?php

class Zend_View_Helper_CleanUrl extends Zend_View_Helper_Abstract
{
   public function cleanUrl( $uristub )
   {
      // set the locale, just once
      setlocale( LC_ALL, "en_US.UTF8" );
      // clean the uristub
      $uristub = iconv( "UTF-8", "ASCII//TRANSLIT", $uristub );
      $uristub = preg_replace( "/[^a-zA-Z0-9\/_|+ -]/", "", $uristub );
      $uristub = preg_replace( "/[\/_|+ -]+/", "-", $uristub );
      $uristub = strtolower( trim( $uristub, "-" ) );

      return $uristub;
   }
}