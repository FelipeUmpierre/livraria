<?php
/**
 * Formatação de Datas
 * Auxiliar da Camada de Visualização
 * 
 * @see APPLICATION_PATH/views/helpers/NumberNormalize.php
 */
class Zend_View_Helper_NumberNormalize extends Zend_View_Helper_Abstract
{
   public function numberNormalize( $value, $precision = 2 )
   {     
      try
      {         
         $locale = new Zend_Locale( "pt_BR" );
         return Zend_Locale_Format::getNumber( $value, array( "locale" => $locale, "precision" => $precision ) );
      }
      catch( Zend_Exception $e )
      {
         return $value;
      }
   }
}