<?php

   /**
    * Formatação de Datas
    * Auxiliar da Camada de Visualização
    * 
    * @see APPLICATION_PATH/views/helpers/Number.php
    */
   class Zend_View_Helper_Number extends Zend_View_Helper_Abstract
   {
   /**
    * Manipulador de Datas
    * @var Zend_Date
    */
   protected static $_currency = null;

   /**
    * Método Principal
    * @param string $value Valor para Formatação
    * @param string $format Formato de Saída
    * @return string Valor Formatado
    */
   public function number( $value, $format = Zend_Currency::NO_SYMBOL )
   {
      try
      {
         $currency = $this->getCurreny();
         return $currency->setFormat( array( "display" => $format ) )->toCurrency( $value );
      }
      catch( Zend_Exception $e )
      {
         return $value;
      }
   }

   /**
    * Acesso ao Manipulador de Datas
    * @return Zend_Date
    */
   public function getCurreny()
   {
   if( self::$_currency == null )
   {
   self::$_currency = new Zend_Currency();
   }

   return self::$_currency;
   }
   }   