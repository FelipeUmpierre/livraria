<?php

class Zend_View_Helper_Url extends Zend_View_Helper_Abstract
{
   public function url( $uri )
   {
      return $this->view->serverUrl( $this->view->baseUrl( $uri ) );
   }
}