<?php

   class Default_MeController extends Livraria_Controller_Application
   {

      public function init()
      {
         parent::init();
      }

      public function indexAction()
      {
         
      }
      
      public function reservedAction()
      {
         $session = $this->getLoginInfo();
         
         $modelReserved = new Default_Model_Reserved();
         
         $reserved = $modelReserved->findAll();
         
         $this->view->reserved = $reserved;
      }
   }   