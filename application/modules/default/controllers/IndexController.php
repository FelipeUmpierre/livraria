<?php

   class Default_IndexController extends Livraria_Controller_Application
   {

      public function init()
      {
         parent::init();
      }

      public function indexAction()
      {
         $modelBook = new Default_Model_Book();

         $books = $modelBook->findAll();

         $this->view->books = $books;
      }
   }   