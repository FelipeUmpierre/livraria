<?php

   class Default_LoginController extends Livraria_Controller_Application
   {

      public function init()
      {
         parent::init();
      }

      public function indexAction()
      {
         $session = $this->getLoginInfo();

         if( isset( $session[ "id" ] ) )
         {
            $this->_redirect( "index" );
         }
      }

      public function authAction()
      {
         $this->disableLayout();

         if( $this->isPost() )
         {
            $params = $this->getPost();
            $sessionKeyName = sprintf( "_%s_session", Livraria_Controller_Configurations::moduleName( $this->getRequest() ) );

            $authorization = new Zend_Session_Namespace( $this->{$sessionKeyName} );

            $dbAdapter = Zend_Db_Table::getDefaultAdapter();
            $authAdapter = new Zend_Auth_Adapter_DbTable( $dbAdapter );

            $authAdapter->setTableName( "users" )
                        ->setIdentityColumn( "email" )
                        ->setCredentialColumn( "password" )
                        ->setCredentialTreatment( "MD5(?)" )
                        ->setIdentity( $params[ "auth" ][ "email" ] )
                        ->setCredential( $params[ "auth" ][ "password" ] )
                        ;

            $authentication = Zend_Auth::getInstance()->authenticate( $authAdapter );

            if( $authentication->isValid() )
            {
               $user = $authAdapter->getResultRowObject( null, array( "password" ) );

               $authorization->{$this->{$sessionKeyName}}[ "id" ] = $user->id;
               $authorization->{$this->{$sessionKeyName}}[ "name" ] = $user->name;
               $authorization->{$this->{$sessionKeyName}}[ "email" ] = $user->email;
               $authorization->{$this->{$sessionKeyName}}[ "cpf" ] = $user->cpf;

               $_SESSION[ $this->{$sessionKeyName} ][ "id" ] = $user->id;
               $_SESSION[ $this->{$sessionKeyName} ][ "name" ] = $user->name;
               $_SESSION[ $this->{$sessionKeyName} ][ "email" ] = $user->email;
               $_SESSION[ $this->{$sessionKeyName} ][ "cpf" ] = $user->cpf;

               $this->_redirect( "index" );
            }
            else
            {
               $this->_helper->FlashMessenger( array( "class" => "error", "message" => "Dados incorretos" ) );
               $this->_redirect( "login" );
            }
         }
      }

      /**
       * Logout
       */
      public function logoutAction()
      {
         $this->disableLayout();

         # apaga a sessão no Zend_Session_Namespace
         $session = new Zend_Session_Namespace( $this->_default_session );
         $session->unsetAll();

         # apaga a sessão da _SESSION
         unset( $_SESSION[ $this->_default_session ][ $this->_default_session ] );

         # apaga a instancia do Zend_Auth
         Zend_Auth::getInstance()->clearIdentity();

         $this->_redirect( "index" );
      }
   }   