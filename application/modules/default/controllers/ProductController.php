<?php

   class Default_ProductController extends Livraria_Controller_Application
   {
      public function init()
      {
         parent::init();
      }

      public function indexAction()
      {
         if( $this->getRequest()->getParam( "id" ) )
         {
            $params = $this->getRequest()->getParams();

            $modelBook = new Default_Model_Book();
            $modelCategory = new Default_Model_Category();
            
            $bookInformations = $modelBook->findById( $params[ "id" ] );
            $relatedBooks = $modelBook->findByRelatedBook( $params[ "id" ], $bookInformations[ "category_id" ] );
            $category = $modelCategory->findAll();
            
            $this->view->book = $bookInformations;
            $this->view->relatedBooks = $relatedBooks;
            $this->view->category = $category;
         }
      }
   }   