<?php

   class Default_CartController extends Livraria_Controller_Application
   {
      protected $sessionNameBook = "cart_books";
      
      public function init()
      {
         parent::init();
      }

      public function indexAction()
      {
         $session = $this->getRemember( $this->sessionNameBook );
         
         if( isset( $session[ "book_id" ] ) && !empty( $session[ "book_id" ] ) )
         {
            $modelBook = new Default_Model_Book();
            
            foreach( $session[ "book_id" ] as $key => $val )
            {
               $bookInformations[] = $modelBook->findById( $val );
            }
            
            $this->view->book = $bookInformations;
         }
      }
      
      public function addAction()
      {
         $this->disableLayout();
         
         if( $this->isPost() )
         {
            $params = $this->getPost();

            $session = new Zend_Session_Namespace( $this->sessionNameBook );
            
            $insert = true;
            
            if( isset( $session->{$this->sessionNameBook}[ "book_id" ] ) && !empty( $session->{$this->sessionNameBook}[ "book_id" ] ) )
            {               
               foreach( $session->{$this->sessionNameBook}[ "book_id" ] as $key => $val )
               {
                  if( in_array( $val, array( $params[ "id" ] ) ) )
                  {
                     $insert = false;
                  }
               }
            }
            
            if( $insert )
            {
               $session->{$this->sessionNameBook}[ "book_id" ][] = $params[ "id" ];
            }
            
            $this->redirector( "index" );
         }
      }
      
      public function removeAction()
      {
         $this->disableLayout();
         
         if( $this->getRequest()->getParam( "id" ) )
         {
            $params = $this->getRequest()->getParams();
            
            $session = $this->getRemember( $this->sessionNameBook );
            
            foreach( $session[ "book_id" ] as $key => $val )
            {
               if( in_array( $val, array( $params[ "id" ] ) ) )
               {
                  unset( $session[ "book_id" ][ $key ] );
               }
            }
            
            $sessionUpdate = new Zend_Session_Namespace( $this->sessionNameBook );
            $sessionUpdate->{$this->sessionNameBook}[ "book_id" ] = $session[ "book_id" ];
            
            $this->redirector( "index" );
         }
      }
      
      public function reserveAction()
      {
         $this->disableLayout();
         
         if( $this->getRequest()->getParam( "h" ) )
         {
            $params = $this->getRequest()->getParams();
            
            $session = $this->getLoginInfo();
            
            try
            {
               if( $params[ "h" ] == md5( $session[ "email" ] . $session[ "id" ] ) )
               {
                  $modelReserved = new Default_Model_Reserved();

                  $bookReserved = $this->getRemember( $this->sessionNameBook );

                  foreach( $bookReserved[ "book_id" ] as $key => $val )
                  {
                     $modelReserved->insert( array(
                        "book_id" => $val,
                        "users_id" => $session[ "id" ],
                        "status_id" => 1 // pendente
                     ) );
                  }

                  $this->cleanRemember( $this->sessionNameBook );

                  $this->_helper->FlashMessenger( array( "class" => "success", "message" => "Reservado com sucesso!" ) );
                  $this->_redirect( "index" );
               }
            }
            catch( Zend_Exception $e )
            {
               printr( $e->getTrace() );
            }
         }
      }
   }   