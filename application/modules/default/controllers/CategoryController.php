<?php

   class Default_CategoryController extends Livraria_Controller_Application
   {

      public function init()
      {
         parent::init();
      }

      public function indexAction()
      {
         $modelBook = new Default_Model_Book();
         $modelCategory = new Default_Model_Category();
         
         $books = $modelBook->findAll();
         
         if( $this->getRequest()->getParam( "id" ) )
         {
            $params = $this->getRequest()->getParams();
            
            $books = $modelBook->findAllByCategoryId( $params[ "id" ] );
            $thisCategory = $modelCategory->findById( $params[ "id" ] );
            
            $this->view->params = $params;
            $this->view->thisCategory = $thisCategory;
         }
         
         $category = $modelCategory->findAll();

         $this->view->books = $books;
         $this->view->category = $category;
      }
   }   