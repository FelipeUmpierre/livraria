<?php

   class Default_Model_Book extends Livraria_Model_App
   {
      protected $_name = "book";
      
      public function findById( $id )
      {
         $sql = $this->select()
                     ->from( $this->_name )
                     ->setIntegrityCheck( false )
                     ->join( "book_description", "book_description.book_id = book.id", array( "mini_description", "description" ) )
                     ->join( "book_language", "book_language.id = book.book_language_id", array( "book_language_name" => "name" ) )
                     ->where( "book.id IN (?)", $id )
                     ;
         
         $return = $this->_db->fetchRow( $sql );
         
         $modelBookAuthor = new Sis_Model_BookAuthor();         
         $bookAuthorInformations = $modelBookAuthor->findByBookId( $return[ "id" ] );
         
         $return[ "author" ] = $bookAuthorInformations;
         
         return $return;
      }
      
      public function findAll()
      {
         $sql = $this->select()
                     ->from( $this->_name )
                     ;

         return $this->_db->fetchAll( $sql );
      }
      
      public function findAllByCategoryId( $id )
      {
         $sql = $this->select()
                     ->from( $this->_name )
                     ->where( "category_id IN (?)", $id )
                     ;

         return $this->_db->fetchAll( $sql );
      }
      
      public function findByRelatedBook( $bookId, $categoryId, $limit = 4 )
      {
         $sql = $this->select()
                     ->from( $this->_name )
                     ->where( "id NOT IN (?)", $bookId )
                     ->where( "category_id IN (?)", $categoryId )
                     ->limit( $limit )
                     ->order( "RAND()" )
                     ;
         
         return $this->_db->fetchAll( $sql );
      }
   }   