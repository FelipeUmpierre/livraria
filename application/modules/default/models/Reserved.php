<?php

   class Default_Model_Reserved extends Livraria_Model_App
   {
      protected $_name = "reserved";
            
      public function findAll()
      {
         $sql = $this->select()
                     ->from( $this->_name )
                     ->setIntegrityCheck( false )
                     ->join( "book", "book.id = reserved.book_id", array( "book_name" => "name", "file_upload" ) )
                     ->join( "status", "status.id = reserved.status_id", array( "status_name" => "name" ) )
                     ;

         return $this->_db->fetchAll( $sql );
      }
   }