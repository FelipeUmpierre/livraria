<?php

   class Default_Model_Category extends Livraria_Model_App
   {
      protected $_name = "category";

      public function findAll()
      {
         $sql = $this->select()
                 ->from( $this->_name )
         ;

         return $this->_db->fetchAll( $sql );
      }
   }   