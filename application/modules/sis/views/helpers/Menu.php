<?php

   class Zend_View_Helper_Menu extends Zend_View_Helper_Abstract
   {

      public function menu()
      {
         return array(
            array(
               "title" => "Home",
               "url" => "index"
            ),
            array(
               "title" => "Reservas",
               "url" => "reserved"
            ),
            array(
               "title" => "Livros",
               "url" => "book",
               "sub" => array(
                  array(
                     "title" => "Listar Livros",
                     "url" => "index"
                  ),
                  array(
                     "title" => "Adicionar Livro",
                     "url" => "new"
                  )
               )
            ),
            array(
               "title" => "Categorias",
               "url" => "category",
               "sub" => array(
                  array(
                     "title" => "Listar Categorias",
                     "url" => "index"
                  ),
                  array(
                     "title" => "Adicionar Categoria",
                     "url" => "new"
                  )
               )
            ),
            array(
               "title" => "Usuários",
               "url" => "users",
               "sub" => array(
                  array(
                     "title" => sprintf( "Usuários do %s", SITE_NAME ),
                     "url" => "index"
                  )
               )
            ),
            array(
               "title" => "Manager",
               "url" => "manager"
            )
         );
      }

   }   