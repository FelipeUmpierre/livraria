<?php

   class Sis_CategoryController extends Livraria_Controller_Application
   {

      public function init()
      {
         parent::init();

         $this->getFlashMessenger();
         $this->generateBreadcrumb( array(
            "title" => "Categorias",
            "icon" => "tags",
            "url" => "category"
         ) );

         $this->view->page = "category";
      }

      public function indexAction()
      {
         $modelCategory = new Sis_Model_Category();

         $categories = $modelCategory->findAll();

         $this->view->subPage = "index";
         $this->view->categories = $categories;
      }

      public function newAction()
      {
         $this->generateBreadcrumb( array(
            "title" => "Adicionando categoria"
         ) );
      }

      public function editAction()
      {
         $this->generateBreadcrumb( array(
            "title" => "Editando categoria"
         ) );

         if( $this->getRequest()->getParam( "id" ) )
         {
            $params = $this->getRequest()->getParams();

            $modelCategory = new Sis_Model_Category();

            $category = $modelCategory->findById( $params[ "id" ] );

            $this->view->category = $category;
         }
      }

      public function createAction()
      {
         $this->disableLayout();

         if( $this->isPost() )
         {
            try
            {
               $params = $this->getPost();

               $modelCategory = new Sis_Model_Category();

               $modelCategory->insert( $params[ "category" ] );

               $this->_helper->FlashMessenger( array( "class" => "success", "message" => "Inserido com sucesso!" ) );
               $this->redirector( "index" );
            }
            catch( Zend_Exception $ex )
            {
               throw new Exception( "Houve algum problema ao tentar inserir esta categoria." );
            }
         }
      }

      public function updateAction()
      {
         $this->disableLayout();

         if( $this->isPost() )
         {
            try
            {
               $params = $this->getPost();

               $modelCategory = new Sis_Model_Category();

               $error = $this->validation( $params );

               if( $error )
               {
                  $this->_helper->FlashMessenger( array( "class" => "danger", "message" => implode( "<br />", $error ) ) );
                  $this->_redirect( sprintf( "sis/category/edit/id/%d", $params[ "id" ] ) );
               }

               $modelCategory->update( $params[ "category" ], $params[ "category" ][ "id" ] );


               $this->_helper->FlashMessenger( array( "class" => "success", "message" => "Atualizado com sucesso!" ) );
               $this->redirector( "index" );
            }
            catch( Zend_Exception $ex )
            {
               throw new Exception( "Houve algum problema ao tentar atualizar esta categoria." );
            }
         }
      }

      protected function validation( array $params )
      {
         $error = "";

         if( !isset( $params[ "category" ][ "name" ] ) )
         {
            $error[] = "Você deve informar o nome da categoria.";
         }

         if( !empty( $error ) )
         {
            return $error;
         }

         return false;
      }
   }   