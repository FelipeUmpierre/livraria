<?php

class Sis_GamesController extends JogosBrasil_Controller_Application
{

   public function init()
   {
      parent::init();

      $this->getFlashMessenger();
      $this->generateBreadcrumb( array(
         "title" => "Jogos",
         "icon" => "gamepad",
         "url" => "games"
      ) );

      $this->view->page = "games";
   }

   public function indexAction()
   {
      $modelGames = new Sis_Model_Games();
      $games = $modelGames->findAll();
      $modelCategory = new Sis_Model_Category();


      $this->view->category = $modelCategory->findAll();
      $this->view->games = $games;
      $this->view->subPage = "index";
   }

   public function newAction()
   {  
      $this->generateBreadcrumb( array(
         "title" => "Adicionando jogo"
      ) );

      $modelSituationsGame = new Sis_Model_SituationsGame();
      $modelSituationsImage = new Sis_Model_SituationsImage();
      $modelSituationsText = new Sis_Model_SituationsText();
      $modelCategory = new Sis_Model_Category();
      $modelManager = new Sis_Model_Manager();

      $authors = $modelManager->findAllByUsersGroupId( 2 );

      $this->view->situationsGame = $modelSituationsGame->findAll();
      $this->view->situationsImage = $modelSituationsImage->findAll();
      $this->view->situationsText = $modelSituationsText->findAll();
      $this->view->category = $modelCategory->findAll();
      $this->view->authors = $authors;
      $this->view->subPage = "new";
   }

   public function editAction()
   {
      $this->generateBreadcrumb( array(
         "title" => "Editando Jogo"
      ) );

      if( $this->getRequest()->getParam( "id" ) )
      {
         $params = $this->getRequest()->getParams();

         $modelGame = new Sis_Model_Games();
         $modelCategory = new Sis_Model_Category();
         $modelGamesDescription = new Sis_Model_GamesDescriptions();
         $modelSituationsText = new Sis_Model_SituationsText();
         $modelSituationsGame = new Sis_Model_SituationsGame();
         $modelSituationsImage = new Sis_Model_SituationsImage();
         $modelManager = new Sis_Model_Manager();

         $authors = $modelManager->findAllByUsersGroupId( 2 );
         $games = $modelGame->findById( $params[ "id" ] );
         $gamesDescription = $modelGamesDescription->findByGameId( $params[ "id" ] );
         $situationsText = $modelSituationsText->findAll();


         $this->view->games = $games;
         $this->view->authors = $authors;
         $this->view->gamesDescription = $gamesDescription;
         $this->view->situationsText = $situationsText;
         $this->view->category = $modelCategory->findAll();
         $this->view->situationsGame = $modelSituationsGame->findAll();
         $this->view->situationsImage = $modelSituationsImage->findAll();
      }
   }

   public function createAction()
   {
      $this->disableLayout();
      try
      {
         if( $this->isPost() )
         {
            $params = $this->getPost();

            $modelGames = new Sis_Model_Games();
            $modelGamesDescription = new Sis_Model_GamesDescriptions();
            $modelDescriptionsType = new Sis_Model_DescriptionsType();
            $modelGamesTags = new Sis_Model_GamesTags();


            $games = $modelGames->insert( $params[ "games" ] );

            $arrTags = explode( ',', $params[ "games" ][ "tags" ] );

            if( $games )
            {
               foreach( $arrTags as $key => $value )
               {
               
                  $modelGamesTags->insert( array( "tags_id" => $value, "games_id" => $games ) );
               }
            
               foreach( $params[ "games_description" ] as $key => $value )
               {

                  $descriptionsType = $modelDescriptionsType->findByName( $key );
                  if( $descriptionsType )
                  {
                     $modelGamesDescription->insert( array( "games_id" => $games, "description" => $value, "descriptions_type_id" => $descriptionsType[ "id" ] ) );
                  }
               }
              
               $this->_helper->FlashMessenger( array( "class" => "success", "message" => "Inserido com sucesso!" ) );
               $this->redirector( "index" );
            }
         }
      }
      catch( Zend_Exception $ex )
      {
         throw new Exception( "Houve algum problema ao tentar inserir este jogo." );
      }
   }

   public function availableAction()
   {
      $this->disableLayout();

      if( $this->isPost() )
      {
         $params = $this->getPost();

         $modelGames = new Sis_Model_Games();

         $gameAvailableName = $modelGames->checkNameGame( $params[ "nome" ] );

         echo $gameAvailableName;
      }
   }

}
