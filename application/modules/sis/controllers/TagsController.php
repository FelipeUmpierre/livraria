<?php

class Sis_TagsController extends JogosBrasil_Controller_Application
{

   public function init()
   {
      parent::init();

      $this->getFlashMessenger();
      $this->generateBreadcrumb( array(
         "title" => "Tags",
         "icon" => "tags",
         "url" => "tags"
      ) );

      $this->view->page = "tags";
   }

   public function indexAction()
   {
      $this->view->subPage = "index";

      $modelTags = new Sis_Model_Tags();

      $this->view->tags = $modelTags->findAll();
   }

   public function newAction()
   {
      $this->generateBreadcrumb( array(
         "title" => "Adicionando tag"
      ) );

      $this->view->subPage = "new";
   }

   public function editAction()
   {

      $this->generateBreadcrumb( array(
         "title" => "Editando tag"
      ) );

      if( $this->getRequest()->getParam( "id" ) )
      {
         $params = $this->getRequest()->getParams();

         $modelTags = new Sis_Model_Tags();

         $tags = $modelTags->findById( $params[ "id" ] );

         $this->view->tags = $tags;
      }
   }

   public function createAction()
   {
      if( $this->isPost() )
      {
         $params = $this->getRequest()->getParams();

         $modelTags = new Sis_Model_Tags();

         $return = JogosBrasil_Upload_Upload::upload( "upload/tags/", $params[ "tags" ][ "name" ] );
         $params[ "tags" ][ "image" ] = $return[ "ok" ];
         $modelTags->insert( $params[ "tags" ] );

         $tags = $modelTags->findNameAndId();
         $tagsJson = Zend_Json::encode( $tags );

         $filename = "json/tags.json";

         if( !$handle = fopen( $filename, 'w+' ) )
         {
            $this->_helper->FlashMessenger( "Erro ao adicionar a Tag!" );
            $this->_redirect( "sis/" );
         }
         if( fwrite( $handle, $tagsJson ) === false )
         {
            $this->_helper->FlashMessenger( "Erro ao adicionar a Tag!" );
            $this->_redirect( "sis/" );
         }

         $this->_helper->FlashMessenger( "Tag adicionada com sucesso!" );
         $this->_redirect( "sis/" );
      }
   }

}
