<?php

class Sis_UsersController extends Livraria_Controller_Application
{

   public function init()
   {
      parent::init();

      $this->getFlashMessenger();
      $this->generateBreadcrumb( array(
         "title" => "Usuários",
         "icon" => "group",
         "url" => "users"
      ) );

      $this->view->page = "users";
   }

   public function indexAction()
   {
      $modelUsers = new Sis_Model_Users();
      $users = $modelUsers->findAll();
      
      $this->view->users = $users;
      $this->view->subPage = "index";
   }

   public function newAction()
   {
      $this->generateBreadcrumb( array(
         "title" => "Adicionando usuário"
      ) );
   }
   
   public function editAction()
   {
      $this->generateBreadcrumb( array(
         "title" => "Editando usuário"
      ) );
      
      if( $this->getRequest()->getParam( "id" ) )
      {
         $params = $this->getRequest()->getParams();
         
         $modelUsers = new Sis_Model_Users();

         $usersInformations = $modelUsers->findById( $params[ "id" ] );
         
         $this->view->usersInformations = $usersInformations;
         $this->view->params = $params;
      }
   }
   
   public function createAction()
   {
      $this->createUpdate( "insert" );
   }
   
   public function updateAction()
   {
      $this->createUpdate( "update" );
   }

   public function createUpdate( $action )
   {
      $this->disableLayout();
      
      if( $this->isPost() )
      {
         $params = $this->getPost();
         
         $modelUsers = new Sis_Model_Users();
         
         try
         {
            if( in_array( $action, array( "update" ) ) )
            {
               $modelUsers->update( $params[ "users" ], $params[ "id" ] );
            }
            else
            {
               $modelUsers->insert( $params[ "users" ] );
            }

            $this->_helper->FlashMessenger( array( "class" => "success", "message" => "Ação efetuada com sucesso!" ) );
         }
         catch( Zend_Exception $e )
         {
            $this->_helper->FlashMessenger( array( "class" => "danger", "message" => "Houve um problema, tente novamente." ) );
         }
         
         $this->redirector( "index" );
      }
   }
}
