<?php

   class Sis_ReservedController extends Livraria_Controller_Application
   {
      public function init()
      {
         parent::init();

         $this->getFlashMessenger();
         $this->generateBreadcrumb( array(
            "title" => "Livros",
            "icon" => "book",
            "url" => "books"
         ) );

         $this->view->page = "reserved";
      }

      public function indexAction()
      {
         $modelReserved = new Sis_Model_Reserved();
         $modelStatus = new Sis_Model_Status();
         
         if( $this->getRequest()->getParam( "users_info" ) )
         {
            $params = $this->getRequest()->getParams();
            
            $reservedInformations = $modelReserved->findAll( $params );
         }
         else
         {
            $reservedInformations = $modelReserved->findAll();
         }
         
         $statusJson = Zend_Json::encode( $modelStatus->findAllForJson() );
         
         $this->view->reserved = $reservedInformations;
         $this->view->status = $statusJson;
      }

      public function newAction()
      {
         $modelCategory = new Sis_Model_Category();
         $modelBookLanguage = new Sis_Model_BookLanguage();

         $categories = $modelCategory->findAll();
         $bookLanguage = $modelBookLanguage->findAll();

         $this->view->categories = $categories;
         $this->view->bookLanguage = $bookLanguage;
      }

      public function editAction()
      {
         if( $this->getRequest()->getParam( "id" ) )
         {
            $params = $this->getRequest()->getParams();

            $modelBook = new Sis_Model_Book();
            $modelCategory = new Sis_Model_Category();
            $modelBookLanguage = new Sis_Model_BookLanguage();
            $modelBookDescription = new Sis_Model_BookDescription();

            $bookInformations = $modelBook->findById( $params[ "id" ] );
            $categories = $modelCategory->findAll();
            $bookLanguage = $modelBookLanguage->findAll();
            $bookDescription = $modelBookDescription->findByBookId( $params[ "id" ] );

            $this->view->book = $bookInformations;
            $this->view->categories = $categories;
            $this->view->params = $params;
            $this->view->bookLanguage = $bookLanguage;
            $this->view->bookDescription = $bookDescription;
         }
      }

      public function createAction()
      {
         
      }

      public function updateAction()
      {
         $this->disableLayout();

         if( $this->isPost() )
         {
            $params = $this->getPost();

            $modelBook = new Sis_Model_Book();
            
            # faz o upload da imagem
            $fileUpload = $this->fileUpload( $params[ "book" ][ "name" ] );
            
            if( isset( $fileUpload[ "ok" ] ) )
            {
               $params[ "book" ][ "file_upload" ] = $fileUpload[ "ok" ];
            }
            
            $modelBook->update( $params[ "book" ], $params[ "id" ] );

            $this->bookDescription( $params );

            $this->_helper->FlashMessenger( array(
               "class" => "success",
               "message" => "Atualizado com sucesso!"
            ) );

            $this->redirector( "index" );
         }
      }

      public function updateStatusAction()
      {
         $this->disableLayout();
         
         if( $this->isPost() )
         {
            $params = $this->getPost();
            
            $modelReserved = new Sis_Model_Reserved();
            $modelReserved->update( array(
               "status_id" => $params[ "value" ],
            ), $params[ "reserved_id" ] );
         }
      }
      
      protected function bookDescription( array $params )
      {
         try
         {
            $modelBookDescription = new Sis_Model_BookDescription();

            $bookDescription = $modelBookDescription->findByBookId( $params[ "id" ] );

            # adiciona ou edita a descrição do livro
            if( !empty( $bookDescription ) )
            {
               $modelBookDescription->update( $params[ "book_description" ], $bookDescription[ "id" ] );
            }
            else
            {
               $params[ "book_description" ] = array_merge( $params[ "book_description" ], array( "book_id" => $params[ "id" ] ) );

               $modelBookDescription->insert( $params[ "book_description" ] );
            }
         }
         catch( Zend_Exception $e )
         {
            printr( $e );
            exit;
         }
      }
      
      protected function fileUpload( $name )
      {
         try
         {
            $fileUpload = Livraria_Upload_Upload::upload( "upload/", $name );
            
            return $fileUpload;
         }
         catch( Zend_Exception $ex )
         {
            printr( $ex->getMessage() );
            exit;
         }
      }
   }   