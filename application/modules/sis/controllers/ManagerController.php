<?php

class Sis_ManagerController extends Livraria_Controller_Application
{

   public function init()
   {
      parent::init();

      $this->getFlashMessenger();
      $this->generateBreadcrumb( array(
         "title" => "Gerenciadores",
         "icon" => "group",
         "url" => "users"
      ) );

      $this->view->page = "manager";
   }

   public function indexAction()
   {
        $this->view->subPage = "index";
   }

}
