<?php

   class Sis_Model_BookLanguage extends Livraria_Model_App
   {
      protected $_name = "book_language";

      public function findAll()
      {
         $sql = $this->select()
                 ->from( $this->_name )
         ;

         return $this->_db->fetchAll( $sql );
      }
   }   