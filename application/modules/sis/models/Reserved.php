<?php

   class Sis_Model_Reserved extends Livraria_Model_App
   {
      protected $_name = "reserved";

      public function findAll( array $params = array() )
      {
         $sql = $this->select()
                     ->from( $this->_name )
                     ->setIntegrityCheck( false )
                     ->join( "users", "users.id = reserved.users_id", array( "users_name" => "name", "users_cpf" => "cpf" ) )
                     ->join( "book", "book.id = reserved.book_id", array( "book_id" => "id", "book_name" => "name" ) )
                     ->join( "status", "status.id = reserved.status_id", array( "status_name" => "name" ) )
                     ;

         if( !empty( $params ) )
         {
            $this->wheres( $sql, $params );
         }
         
         $result = $this->_db->fetchAll( $sql );
         
         return $result;
      }

      public function findById( $id )
      {
         $sql = $this->select()
                 ->from( $this->_name )
                 ->where( "id = ?", $id )
         ;

         return $this->_db->fetchRow( $sql );
      }

      public function wheres( &$sql, array $params )
      {
         try
         {
            if( isset( $params[ "users_info" ] ) )
            {
               $sql->where( "users.cpf LIKE ? OR users.name LIKE ? or users.email LIKE ?", is_numeric( $params[ "users_info" ] ) ? $params[ "users_info" ] : sprintf( "%%%s%", $params[ "users_info" ] ) );
            }
         }
         catch( Zend_Exception $ex )
         {

         }
      }
   }   