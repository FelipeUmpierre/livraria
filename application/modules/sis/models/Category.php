<?php

   class Sis_Model_Category extends Livraria_Model_App
   {
      protected $_name = "category";

      public function findAll()
      {
         $sql = $this->select()
                 ->from( $this->_name )
         ;

         return $this->_db->fetchAll( $sql );
      }

      public function findById( $id )
      {
         $sql = $this->select()
                 ->from( $this->_name )
                 ->where( "id = ?", $id )
         ;

         return $this->_db->fetchRow( $sql );
      }

      public function countAll()
      {
         $sql = $this->select()
                 ->from( $this->_name )
         ;

         return count( $this->_db->fetchAll( $sql ) );
      }
   }   