<?php

   class Sis_Model_CategoryDescriptions extends JogosBrasil_Model_App
   {
      protected $_name = "category_descriptions";

      public function findByCategoryId( $categoryId )
      {
         $sql = $this->select()
                 ->from( $this->_name, array( "description" ) )
                 ->setIntegrityCheck( false )
                 ->join( "descriptions_type", "descriptions_type.id = category_descriptions.descriptions_type_id", array( "descriptions_type_name" => "name" ) )
                 ->where( "category_id = ?", $categoryId )
         ;

         $result = $this->_db->fetchAll( $sql );
         $array = array();

         foreach( $result as $key => $val )
         {
            $array[ $val[ "descriptions_type_name" ] ] = $val;
         }

         return $array;
      }

      public function findByCategoryIdDescriptionsType( $categoryId, $descriptionsType )
      {
         $sql = $this->select()
                 ->from( $this->_name )
                 ->setIntegrityCheck( false )
                 ->join( "descriptions_type", "descriptions_type.id = category_descriptions.descriptions_type_id", array( "descriptions_type_name" => "name" ) )
                 ->where( "category_id = ?", $categoryId )
                 ->where( "descriptions_type.id = ?", $descriptionsType )
         ;

         $result = $this->_db->fetchRow( $sql );


         return $result;
      }
   }   