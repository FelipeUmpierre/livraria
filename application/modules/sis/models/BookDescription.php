<?php

   class Sis_Model_BookDescription extends Livraria_Model_App
   {
      protected $_name = "book_description";

      public function findByBookId( $bookId )
      {
         $sql = $this->select()
                 ->from( $this->_name )
                 ->where( "book_id = ?", $bookId )
         ;

         return $this->_db->fetchRow( $sql );
      }
   }   