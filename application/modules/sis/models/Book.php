<?php

   class Sis_Model_Book extends Livraria_Model_App
   {
      protected $_name = "book";

      public function findAll()
      {
         $sql = $this->select()
                 ->from( $this->_name )
                 ->setIntegrityCheck( false )
                 ->join( "category", "category.id = book.category_id", array( "category_name" => "name" ) )
         ;

         $result = $this->_db->fetchAll( $sql );

         foreach( $result as $key => $val )
         {
            $result[ $key ][ "author" ] = $this->findAuthorFromBookId( $val[ "id" ] );
         }

         return $result;
      }

      public function findById( $id )
      {
         $sql = $this->select()
                 ->from( $this->_name )
                 ->where( "id = ?", $id )
         ;

         return $this->_db->fetchRow( $sql );
      }

      public function findAuthorFromBookId( $bookId )
      {
         $sql = $this->select()
                 ->from( "book_author", array() )
                 ->setIntegrityCheck( false )
                 ->join( "author", "author.id = book_author.author_id", array( "author_name" => "name" ) )
                 ->where( "book_author.book_id = ?", $bookId )
         ;

         return $this->_db->fetchAll( $sql );
      }
      
      public function findBySearch( array $params )
      {
         $sql = $this->select()
                 ->from( $this->_name )
                 ->setIntegrityCheck( false )
                 ->join( "category", "category.id = book.category_id", array( "category_name" => "name" ) )
         ;
         
         if( isset( $params[ "name" ] ) && !empty( $params[ "name" ] ) )
         {
            $sql->where( "book.name LIKE ?", "%" . $params[ "name" ] . "%" );
         }
         
         if( isset( $params[ "category_id" ] ) && !empty( $params[ "category_id" ] ) )
         {
            $sql->where( "book.category_id = ?", $params[ "category_id" ] );
         }
         
         $result = $this->_db->fetchAll( $sql );

         foreach( $result as $key => $val )
         {
            $result[ $key ][ "author" ] = $this->findAuthorFromBookId( $val[ "id" ] );
         }

         return $result;
      }
   }   