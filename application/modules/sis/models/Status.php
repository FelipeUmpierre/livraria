<?php

   class Sis_Model_Status extends Livraria_Model_App
   {
      protected $_name = "status";

      public function findAll()
      {
         $sql = $this->select()
                     ->from( $this->_name )
                     ;

         return $this->_db->fetchAll( $sql );
      }
      
      public function findAllForJson()
      {
         $sql = $this->select()
                     ->from( $this->_name )
                     ;
         
         $result = $this->_db->fetchAll( $sql );
         
         $json = array();
         
         foreach( $result as $key => $val )
         {
            $json[ "status" ][] = array(
               "value" => $val[ "id" ],
               "text" => $val[ "name" ]
            );
         }
         
         return $json;
      }
   }   