$( function() {
    var menu = Array();
    var search = Array();
    var dropdown = Array();

    /* DROPDOWN MENU */
    $(".bt-tags").click(function (e) {
        e.preventDefault();
        if (menu[0] === 0 || menu[0] === undefined) {
            $(".ico").addClass("active")
            $("#bt-tags").addClass("active")
        } else {
            $(".ico").removeClass("active")
            $("#bt-tags").removeClass("active")
        }
        $(".menu-dropdown-content").animate({
            height: "toggle"
        }, 400, function () {
            if (menu[0] === 0 || menu[0] === undefined) {
                $(".menu-dropdown-content").css("display", "block");
                menu[0] = 1
            } else {
                $(".menu-dropdown-content").css("display", "none");
                delete menu[0]
            }
        })
    })

    /* DROPDOWN USUÁRIO */
    $(".op-dropdown").click(function (e) {
        e.preventDefault();
        if (dropdown[0] === 0 || dropdown[0] === undefined) {
            $(".user").addClass("active");
            $(".user-dropdown").addClass("active");
        } else {
            $(".user").removeClass("active");
            $(".user-dropdown").removeClass("active");
        }
        $(".dropdown").fadeIn( "0", function () {
            if (dropdown[0] === 0 || dropdown[0] === undefined) {
                $(".xp-dropdown").css("display", "inline-block");
                $(".dropdown").css("display", "block");
                dropdown[0] = 1
            } else {
                $(".xp-dropdown").css("display", "none");
                $(".dropdown").css("display", "none");
                delete dropdown[0]
            }
        })
    })

    /* BUSCA */
    $(".search-button").click(function (e) {
        e.preventDefault();
        $(".hide-for-search").addClass("hide");
        $("#search").animate({ width: 'show' }, 450, function(){
            $( "#search-input" ).focus();
        }); 
    });
    $(".close-search").click(function (e) {
        e.preventDefault();
        $("#search").animate({ width: 'hide' }, 450, function(){
            $(".hide-for-search").removeClass("hide");
        }); 
    })

    /* */


    $( window ).scroll( function( e ) {

        var scroll = $( this ).scrollTop();
        var a = 150;

        if( scroll >= a )
        {
            $( "body" ).addClass( "header-scroll" );
            $( "#header" ).addClass( "header-scroll" );
            $( ".menu-scroll" ).css( "display", "block" );
        }
        else if( scroll < a )
        {
            $( "body" ).removeClass( "header-scroll" );
            $( "#header" ).removeClass( "header-scroll" );
            $( ".menu-scroll" ).css( "display", "none" );
        }
    } );

    $( ".timeline-progressbar" ).css( "height", $( ".timeline-container" ).height() );
    $( ".center-div" ).css( "width", $( ".center-div" ).width() );
} );