if( typeof statusReservedJsonJS !== 'undefined' )
{
   $( ".change-status-reserve" ).editable( {
      mode: "inline",
      type: "select",
      params: function( params ) {
         params.reserved_id = $( this ).editable().data( "reserved-id" );

         return params;
      },
      url: DEFAULT + "sis/reserved/update-status",
      showbuttons: false,
      source: statusReservedJsonJS[ "status" ],
      inputclass: "form-control input-medium"
   } );
}