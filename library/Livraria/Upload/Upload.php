<?php

class Livraria_Upload_Upload
{

   public static function upload( $path = "upload/", $name, $required = true, $keyFileUploadField = "" )
   {
      $adapter = new Zend_File_Transfer_Adapter_Http();
      $adapter->setDestination( $path );

      $return = array();

      if( isset( $_FILES[ sprintf( "%sfile", $keyFileUploadField ) ] ) && $_FILES[ sprintf( "%sfile", $keyFileUploadField ) ][ "error" ] !== 4 )
      {
         $ext = pathinfo( $adapter->getFileName( sprintf( "%sfile", $keyFileUploadField ), false ) );
         $filename = sprintf( "%s.%s", self::cleanUrl( $name ), $ext[ "extension" ] );

         $filterRename = new Zend_Filter_File_Rename( array( "target" => $path . $filename, "overwrite" => true ) );
         $adapter->addFilter( $filterRename );

         $upload = new Zend_File_Transfer();
         $files = $upload->getFileInfo();

         foreach( $files as $file => $info )
         {
            if( !$adapter->isUploaded( $file ) )
            {
               if( $required )
               {
                  $return[ "error" ] = "Você deve enviar um arquivo para upload. Tente novamente";
               }
               else
               {
                  $return[ "ok" ] = "ok";
               }
            }

            if( !$adapter->isValid( $file ) )
            {
               $return[ "error" ] = "Arquivo que foi feito o upload é de formato inválido. Tente novamente.";
            }

            if( $adapter->receive() )
            {
               $return[ "ok" ] = $filename;
            }
         }
      }
      else
      {
         if( $required )
         {
            $return[ "error" ] = "Você deve enviar um arquivo para upload. Tente novamente.";
         }
         else
         {
            $return[ "ok" ] = "ok";
         }
      }

      return $return;
   }

   public static function cleanUrl( $uristub )
   {
      // set the locale, just once
      setlocale( LC_ALL, "en_US.UTF8" );
      // clean the uristub
      $uristub = iconv( "UTF-8", "ASCII//TRANSLIT", $uristub );
      $uristub = preg_replace( "/[^a-zA-Z0-9\/_|+ -]/", "", $uristub );
      $uristub = preg_replace( "/[\/_|+ -]+/", "-", $uristub );
      $uristub = strtolower( trim( $uristub, "-" ) );

      return $uristub;
   }

}
