<?php

class Livraria_Model_App extends Zend_Db_Table
{
   protected $_name;
   protected $_primary = "id";

   public function insert( Array $_data )
   {
      try
      {
         $_data[ "created_at" ] = date( "Y-m-d H:i:s" );

         return parent::insert( $this->tableColumms( $_data ) );
      }
      catch( Zend_Db_Exception $e )
      {
         print( $e->getMessage() );
      }
   }

   /**
    * Método para editar dados do banco de dados
    * 
    * @param array $_data array dos dados a serem editados
    * @param type $_id pode ser array, se for, determinar na chave qual o campo, o valor para a edição, ex: array( "meu_id" => 3 );
    * @return type 
    */
   public function update( Array $_data, $_id )
   {
      try
      {
         if( is_array( $_id ) )
         {
            $_where = array();

            foreach( $_id as $_key => $_val )
            {
               $_where[] = $this->getAdapter()->quoteInto( sprintf( "%s = ?", $_key ), $_val );
            }
         }
         else
         {
            $_where = $this->getAdapter()->quoteInto( "id = ?", $_id );
         }

         return parent::update( $this->tableColumms( $_data ), $_where );
      }
      catch( Zend_Db_Exception $e )
      {
         print( $e->getMessage() );
      }
   }

   public function delete( $_id )
   {
      try
      {
         if( is_array( $_id ) )
         {
            foreach( $_id as $_key => $_val )
            {
               $_where[] = $this->getAdapter()->quoteInto( sprintf( "%s = ?", $_key ), $_val );
            }
         }
         else
         {
            $_where = $this->getAdapter()->quoteInto( "id = ?", $_id );
         }

         return parent::delete( $_where );
      }
      catch( Zend_Db_Exception $e )
      {
         print( $e->getMessage() );
      }
   }

   /**
    * Método para verificar se as colunas vindas para a inserção
    * na tabela existem, se não existir
    * ele não lista
    * 
    * @param array $_data dados a serem verificados
    * @return array 
    */
   private function tableColumms( Array $_data )
   {
      $_cols = array();

      foreach( $_data AS $_columns => $_value )
      {
         if( in_array( $_columns, $this->_getCols() ) )
         {
            $_cols[ $_columns ] = $_value;
         }
      }

      return $_cols;
   }

    /**
    * Retorna todos os registros da tabela
    * 
    * @return array
    */
   public function findAll()
   {
      $sql = $this->select()
              ->from( $this->_name )
      ;

      return $this->_db->fetchAll( $sql );
   }

   /**
    * Retorna o id do usuário logado
    * 
    * @return int
    */
   public function getIdLogged()
   {
      $_id = Zend_Auth::getInstance()->getStorage()->read();
      return $_id->id;
   }

   /**
    * Busca pelo id na model desejada
    * 
    * @param int $id id do registro / pode ser array com os ids
    * @return array
    */
   public function findById( $id )
   {
      $sql = $this->select()
                  ->from( $this->_name )
                  ->where( "id IN ( ? )", $id )
                  ;

      if( is_array( $id ) )
      {
         return $this->_db->fetchAll( $sql );
      }

      return $this->_db->fetchRow( $sql );
   }

}
