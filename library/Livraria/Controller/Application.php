<?php

   /**
    * Classe com funções padrões
    * 
    * @author Felipe Pieretti Umpierre <umpierre[dot]felipe[at]gmail[dot]com>
    */
   class Livraria_Controller_Application extends Zend_Controller_Action
   {
      protected $_default_session = "lv_default";
      protected $_sis_session = "lv_sis";
      protected static $_session_informations;
      
      public function init()
      {
         $modelCategory = new Default_Model_Category();

         $categories = $modelCategory->findAll();

         $this->view->categories = $categories;
         
         $this->getLoginInfo();
      }
      
      /**
       * Verifica se o usuário que está tentando acessar determinada página está logado
       * 
       * @param string $return URL para retorno, caso necessário
       */
      protected function isLogged( $return = null )
      {
         $session = $this->getLoginInfo();

         if( !isset( $session[ "id" ] ) )
         {
            if( is_null( $return ) )
            {
               $this->_redirect( sprintf( "%s/login/logout", Vcreditos_Controller_Configurations::moduleName( $this->getRequest() ) ) );
            }
            else
            {
               $this->_redirect( sprintf( "%s/login/?url=%s", Vcreditos_Controller_Configurations::moduleName( $this->getRequest() ), $return ) );
            }
         }
      }

      /**
       * Verifica se a requisição é POST
       * 
       * @return bool
       */
      public function isPost()
      {
         return $this->_request->isPost();
      }

      /**
       * Verifica se a requisição é GET
       * 
       * @return bool
       */
      public function isGet()
      {
         return $this->_request->isGet();
      }

      /**
       * Retorna os valores vindos do POST
       * 
       * @return array
       */
      public function getPost()
      {
         return $this->_request->getPost();
      }

      /**
       * Retorna os valores vindos do GET
       * 
       * @return array
       */
      public function getQuery()
      {
         return $this->_request->getQuery();
      }

      /**
       * Desativa o layout
       */
      public function disableLayout()
      {
         $this->_helper->layout->disableLayout();
      }

      /**
       * Retorna para uma página da mesma controller
       * 
       * @param string $_view 
       */
      public function redirector( $_view )
      {
         $this->_helper->redirector( $_view );
      }

      /**
       * Muda o layout da página
       * 
       * @param string $_layout
       */
      public function setLayout( $_layout )
      {
         $this->_helper->_layout->setLayout( $_layout );
      }

      /**
       * Verifica se existe alguma mensagem via Flash,
       * e adiciona à variável $this->view->information
       */
      protected function getFlashMessenger()
      {
         if( $this->_helper->FlashMessenger->hasMessages() )
         {
            $this->view->information = $this->_helper->FlashMessenger->getMessages();
         }
      }

      /**
       * Função para permitir o acesso apenas de usuário expecificos.
       * 
       * @param array $id array com os ids dos tipos de usuários que podem ter acesso ao arquivo
       * @param string $urlReturn URL para retorno, caso o usuário não possua permissão
       */
      protected function accessOnly( $id, $urlReturn = "sis/index" )
      {
         $session = $this->getLoginInfo();

         if( in_array( $session[ "users_group_id" ], array( 1 ) ) )
         {
            $this->_redirect( "sis/login/logout" );
         }

         if( !in_array( $session[ "users_group_id" ], $id ) )
         {
            $this->_helper->FlashMessenger( array( "message" => "Você não possui acesso !" ) );
            $this->_redirect( sprintf( "%s", $urlReturn ) );
         }
      }

      /**
       * Gera o breadcrumb
       * 
       * @param array $breadcrumb array para o breadcrumb
       * @return void
       */
      protected function generateBreadcrumb( array $breadcrumb )
      {
         $this->_breadcrumb[] = $breadcrumb;

         $this->view->breadcrumb = $this->_breadcrumb;
      }

      /**
       * Pega as informações da sessão do usuário logado
       * gera uma variável acessável para view ->login
       * 
       * @return array
       */
      public function getLoginInfo()
      {
         $sessionKeyName = sprintf( "_%s_session", Livraria_Controller_Configurations::moduleName( $this->getRequest() ) );

         $session = new Zend_Session_Namespace( $this->{$sessionKeyName} );
         $session = $session->{$this->{$sessionKeyName}};
         
         if( !isset( $session[ "id" ] ) )
         {
            $session = $_SESSION[ $this->{$sessionKeyName} ][ $this->{$sessionKeyName} ];
         }

         $this->view->login = $session;
         self::$_session_informations = $session;
         return $session;
      }

      /**
       * Retorna o id do usuário logado
       * 
       * @return int
       */
      public static function getIdFromUserLogged()
      {
         return self::$_session_informations[ "id" ];
      }

      /**
       * Seta valores em uma sessão
       * 
       * @param string $sessionRemeberName nome da sessão
       * @param array $remember
       */
      public function setRemember( $sessionRemeberName, array $params )
      {
         $remeberSession = new Zend_Session_Namespace( $sessionRemeberName );

         foreach( $params as $key => $val )
         {
            $remeberSession->{$sessionRemeberName}[ $key ] = $val;
         }

         return $this;
      }

      /**
       * Retorna valores de uma sessão
       * 
       * @param string $sessionRemeberName nome da sessão
       * @return array
       */
      public function getRemember( $sessionRemeberName )
      {
         $remeberSession = new Zend_Session_Namespace( $sessionRemeberName );
         return $remeberSession->{$sessionRemeberName};
      }

      /**
       * Limpa as informações de uma sessão
       * 
       * @param string $sessionRemeberName nome da sessão
       */
      public function cleanRemember( $sessionRemeberName )
      {
         $remeberSession = new Zend_Session_Namespace( $sessionRemeberName );
         $remeberSession->unsetAll();

         return $this;
      }
      
      /**
       * Verifica se a senha que foi colocado pelo usuário
       * 
       * @return json Retorna JSON de "ok" ou de "error"
       */
      public function passwordVerification( $params )
      {
         $session = $this->getLoginInfo();

         $modelUser = new User_Model_Users();

         $user = $modelUser->findById( $session[ "id" ] );
         
         if( $user[ "password" ] == md5( $params[ "password" ] ) )
         {
            return Zend_Json::encode( array( "ok" => "Senha Ok !" ) );
         }
         else
         {
            return Zend_Json::encode( array( "error" => sprintf( "Senha %s incorreta!", VCREDITOS_NAME ) ) );
         }
      }

      /**
       * Troca os espaços de uma string por um "-"
       */
      public function toAscii($str) {
         $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
         $clean = preg_replace("/[^a-zA-Z0-9\/_| -]/", '', $clean);
         $clean = strtolower(trim($clean, '-'));
         $clean = preg_replace("/[\/_| -]+/", '-', $clean);

         return $clean;
      }
   }   