<?php
   
   /**
    * Classo para a identificação do modulo.
    * 
    * @author Felipe Pieretti Umpierre <umpierre[dot]felipe[at]gmail[dot]com>
    */
   class Livraria_Controller_Configurations extends Zend_Controller_Plugin_Abstract
   {
      public static function moduleName( Zend_Controller_Request_Abstract $request )
      {
         return $request->getModuleName();
      }
   }   