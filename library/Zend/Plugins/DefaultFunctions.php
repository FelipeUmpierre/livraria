<?php

class Zend_Plugins_DefaultFunctions extends Zend_Controller_Action 
{
   protected $_pendent = array( 
      1 => array( 0, 1, 2, 5, 6 ),
      2 => array( 0, 1, 4, 5, 7 )
   );

   protected $_completed = array(
      1 => array( 3 ),
      2 => array( 2 ),
      5 => array( 3 ),
      6 => array( 2 ),
      7 => array( 3 ),
      8 => array( 2 )
   );

   protected $_canceled = array(            
      1 => array( 4 ),
      2 => array( 3, 6 ),
      5 => array( 4 ),
      6 => array( 3 ),
      7 => array( 4 ),
      8 => array( 3 )
   );

   # quais situações terão envio de e-mail 
   protected $_email = array(
      1 => array( 0, 1, 5, 6 ),
      2 => array( 0, 5 ),
      5 => array( 5, 6 ),
      6 => array( 5, 6 )
   );

   protected $_fileUpload = array(
      2 => array( 2 )
   );
    
	public function getSituationPurchase( $return = array() )
   {
      $array = array( 
         0 => array(
            "title" => "Pendente",
            "color" => "#222222",
            "ok" => array( 2 ),
            "error" => array( 4 ),
            "permission" => array( 3, 5 )
         ),
         1 => array(
            "title" => "Verificando",
            "color" => "#FF9900",
            "ok" => array( 2 ),
            "error" => array( 4, 5 ),
            "permission" => array( 3, 5 )
         ), 
         2 => array(
            "title" => "Aguardando Resposta BET365",
            "color" => "#0055DD",
            "required" => true,
            "ok" => array( 3 ),
            "error" => array( 4, 6 ),
            "permission" => array( 3, 5 )
         ),
         3 => array( 
            "title" => "Concluído",
            "color" => "#009900"
         ), 
         4 => array(
            "title" => "Cancelado",
            "color" => "#CC0000",
            "permission" => array( 3, 5 )
         ),
         5 => array(
            "title" => "Não Encontrado",
            "color" => "#CC0000",
            "ok" => array( 2 ),
            "error" => array( 4 ),
            "permission" => array( 3, 5, 6 )
         ), 
         6 => array(
            "title" => "Contate a BET365",
            "color" => "#CC0000",
            "ok" => array( 2 ),
            "error" => array( 4 ),
            "permission" => array( 3, 5, 6 )
         )
      );

      $newArray = array();

      if( !empty( $return ) )
      {
         foreach( $array as $key => $val )
         {
            if( in_array( $key, $return ) )
            {
               $newArray[ $key ] = $val;
            }
         }
      }
     
      return empty( $newArray ) ? $array : $newArray;
   }

	public function getSituationRedemption( $return = array() )
   {
      $array = array( 
         0 => array(
            "title" => "Pendente",
            "color" => "#222222",
            "ok" => array( 7 ),
            "error" => array( 3, 6 ),
            "permission" => array( 3, 5 ),
            "header" => "Solicitar Transferência"
         ),
         1 => array(
            "title" => "Processando",
            "color" => "#0055DD",
            "ok" => array( 2 ),
            "error" => array( 3, 4 ),
            "required" => true,
            "permission" => array( 3, 5 ),
            "header" => "Confirmar Depósito ?"
         ),
         2 => array(
            "title" => "Concluído",
            "color" => "#009900",
            "file" => true,
            "permission" => array( 3, 5 ),
             "header" => "Concluido"
         ), 
         3 => array(
            "title" => "Cancelado",
            "color" => "#CC0000",
            "permission" => array( 3, 5 )
         ),
         4 => array(
            "title" => "Contate o Suporte",
            "color" => "#CC0000",
            "ok" => array( 2 ),
            "error" => array( 3 ),
            "permission" => array( 3, 5, 6 ),
            "header" => "Confirmar Depósito ?"
         ), 
         5 => array(
            "title" => "Contate o Suporte da BET365",
            "color" => "#CC0000",
            "ok" => array( 7 ),
            "error" => array( 3 ),
            "permission" => array( 3, 5 ),
            "header" => "Solicitar Transferência ?"                
         ),
         6 => array(
            "title" => "Saldo Insuficiente",
            "color" => "#CC0000",
            "permission" => array( 3, 5 )
         ),
         7 => array(
            "title" => "Aguardando Resposta BET365",
            "title_front" => "Pendente",
            "color" => "#0055DD",
            "ok" => array( 1 ),
            "error" => array( 3, 5, 6 ),
            "permission" => array( 3, 5 ),
            "header" => "Confirmar Transferência ?"
         )
          
      );

      $newArray = array();

      if( !empty( $return ) )
      {
         foreach( $array as $key => $val )
         {
            if( in_array( $key, $return ) )
            {
               $newArray[ $key ] = $val;
            }
         }
      }

      return empty( $newArray ) ? $array : $newArray;
   }

   protected function getPendentSituation()
   {
      return $this->_pendent;
   }

   protected function getCanceledSituation()
   {
      return $this->_canceled;
   }

   protected function getCompletedSituation()
   {
      return $this->_completed;
   }

   public function getEmailFromApplicationIni( $key )
   {
      $emailConfiguration = Zend_Controller_Front::getInstance()->getParam( "bootstrap" );
      $emails = $emailConfiguration->getOption( "sendEmail" );
      
      return empty( $key ) ? $emails : $emails[ $key ];
   }

   public function getNetellerInfoFromApplicationIni( $key )
   {
      $netellerConfiguration = Zend_Controller_Front::getInstance()->getParam( "bootstrap" );
      $neteller = $netellerConfiguration->getOption( "neteller" );
      
      return empty( $key ) ? $neteller : $neteller[ $key ];
   }
}