<?php

/** @see Zend_View_Helper_Abstract */
require_once "Zend/View/Helper/Abstract.php";

class Zend_View_Helper_TranslateDate extends Zend_View_Helper_Abstract
{
   protected $_months = array(
      1 => "Janeiro",
      2 => "Fevereiro",
      3 => "Março",
      4 => "Abril",
      5 => "Maio",
      6 => "Junho",
      7 => "Julho",
      8 => "Agosto",
      9 => "Setembro",
      10 => "Outubro",
      11 => "Novembro",
      12 => "Dezembro"
   );

   protected $_value;


   /**
    * @var $value valor a ser passado
    */
   public function translateDate( $value )
   {
      $this->_value = $value;
      return $this;
   }

   public function month()
   {
      return isset( $this->_months[ $this->_value ] ) ? $this->_months[ $this->_value ] : "Mês inválido";
   }

}